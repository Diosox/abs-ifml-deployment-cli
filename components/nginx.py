import os
import shutil
import traceback
from utils.administration import APP_ROOT_PATH
from utils.ports import release_port
from utils.shell import PLATFORM, run_process
from utils.template import fill_template


class NginxInstance():
    def __init__(self, payload, config):
        self.config = config
        self.payload = dict(payload)
        self.NGINX_PATH = os.path.abspath(
            os.path.expanduser(self.config["work_paths::nginx_location"]))
        self.SOURCE_PATH = os.path.join(APP_ROOT_PATH, "templates")
        self.DEPLOY_ROOT = os.path.abspath(
            os.path.expanduser(self.config["work_paths::deploy_location"]))
        self.PRODUCT_ROOT = os.path.join(self.DEPLOY_ROOT,
                                         self.payload["product_name"])
        self.DEPLOY_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                            self.config["logs::nginx_deploy"])
        self.RUN_LOG_PATH = os.path.join(self.PRODUCT_ROOT,
                                         self.config["logs::nginx_run"])
        self.nginx_product_path = os.path.abspath(
            os.path.join(self.PRODUCT_ROOT, "nginx"))
        self.html_files = [("maintenance.template.html", "maintenance.html"),
                           ("404.template.html", "404.html")]
        self.payload.update({
            "nginx_product_path":
            self.nginx_product_path,
            "access_log":
            os.path.join(self.PRODUCT_ROOT, self.config["logs::nginx_access"]),
            "error_log":
            os.path.join(self.PRODUCT_ROOT, self.config["logs::nginx_error"]),
            "contact_person":
            config.get("email::admin_emails", "").split(",")[0],
        })
        if PLATFORM == "win32":
            self.conf_destination = os.path.join(
                self.NGINX_PATH, "sites",
                self.payload["product_name"] + ".conf")
        else:
            self.conf_destination = os.path.join(
                self.NGINX_PATH, self.payload["product_name"] + ".conf")

    def build(self):
        try:
            if not os.path.exists(os.path.dirname(self.DEPLOY_LOG_PATH)):
                os.makedirs(os.path.dirname(self.DEPLOY_LOG_PATH),
                            exist_ok=True)
            if not os.path.exists(self.nginx_product_path):
                os.makedirs(self.nginx_product_path)

            self.__apply_templates()
            self.__execute_certbot()
        except Exception:
            traceback.print_exc()
            return False
        return True

    def is_running(self):
        return True  # NGINX instance always run

    def run(self, socket):
        release_port(socket)
        if PLATFORM == "win32":
            self.NGINX_PATH = os.path.abspath(
                os.path.expanduser(self.config["work_paths::nginx_location"]))
            cmd = [os.path.join(self.NGINX_PATH, "nginx.exe"), "-s", "reload"]
            run_process(cmd, cwd=self.NGINX_PATH)
        elif PLATFORM == "linux":
            with open(self.RUN_LOG_PATH, "a") as f:
                cmd = ["sudo", "systemctl", "reload", "nginx"]
                run_process(cmd, log=f)

    def stop(self):
        pass  # NGINX instance shouldn't be stopped

    def update(self):
        pass  # NGINX instance can't be updated

    def revert(self):
        pass  # NGINX instance can't be updated so no need to revert

    def destroy(self):
        try:
            os.remove(self.conf_destination)
        except IOError:
            pass
        shutil.rmtree(os.path.abspath(os.path.join(self.PRODUCT_ROOT,
                                                   "nginx")),
                      ignore_errors=True)
        self.run(None)  # refresh with conf removed

    def __apply_templates(self):
        shutil.copy(os.path.join(self.SOURCE_PATH, "nginx.template.conf"),
                    self.conf_destination)
        fill_template(self.conf_destination, self.payload)
        for x in self.html_files:
            html_destination = os.path.join(self.nginx_product_path, x[1])
            shutil.copy(os.path.join(self.SOURCE_PATH, x[0]), html_destination)
            fill_template(html_destination, self.payload)

    def __execute_certbot(self):
        is_linux = PLATFORM == "linux"
        is_not_localhost = self.payload["domain_name"] != "localhost"
        is_ssl_enabled = self.payload["enable_ssl"]
        is_certbot_enabled = self.config[
            "general::generate_ssl_certificate"].lower() == "true"
        if is_linux and is_not_localhost and is_ssl_enabled and is_certbot_enabled:
            with open(self.DEPLOY_LOG_PATH, "a") as f:
                ssl_cmd = [
                    "sudo", "certbot", "run", "--nginx", "-n", "--reinstall",
                    "-d", self.payload["product_address"]
                ]
                ssl_returncode = run_process(ssl_cmd, wait=True, log=f)
            if ssl_returncode != 0:
                raise IOError(
                    "SSL certificate generator failed with error code: %s" %
                    (ssl_returncode, ))
