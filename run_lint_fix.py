#!/usr/bin/env python
import os
import subprocess

# Check for directories
EXCLUDE = [
    ".env",
    ".git",
    ".vscode",
    "coverage",
    "static",
    "templates",
    "tests",
    "src",
    "dist",
    "node_modules",
    "public",
    "env",
    "_base",
    "_log_backup",
    "_nginx",
    "_node",
    "products"
]
DIRS = [x for x in os.listdir() if x not in EXCLUDE and os.path.isdir(x)]
DIRS.append("manage.py")

subprocess.call(["yapf", "-r", "-i"] + DIRS)
