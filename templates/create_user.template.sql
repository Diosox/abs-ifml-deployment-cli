{% if db_type.lower() == "postgresql" %}
DROP ROLE IF EXISTS {{product_subdomain}};
CREATE ROLE {{product_subdomain}} WITH LOGIN ENCRYPTED PASSWORD '{{db_password}}';
GRANT ALL PRIVILEGES ON DATABASE {{product_subdomain}} TO {{product_subdomain}};
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {{product_subdomain}};
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO {{product_subdomain}};
{% else %}
CREATE USER IF NOT EXISTS '{{product_subdomain}}' IDENTIFIED BY '{{db_password}}';
ALTER USER IF EXISTS '{{product_subdomain}}' IDENTIFIED BY '{{db_password}}';
GRANT ALL PRIVILEGES ON `{{product_subdomain}}`.* TO '{{product_subdomain}}';
FLUSH PRIVILEGES;
{% endif %}
