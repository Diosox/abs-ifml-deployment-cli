Subject: {{generator_name}} - "{{original_product_name}}" Product Creation Started
From: {{contact_person}}
To: {{user.email}}

Hai, {{user.name}}{% if user.surname %} {{user.surname}}{% endif %}!

{{generator_name}} sedang membangun produk Anda yang bernama: {{original_product_name}}.
Mohon bersabar, ini akan memakan waktu beberapa saat.
Jika proses telah selesai, {{generator_name}} akan memberitahu Anda kembali lewat email.

Jika setelah 24 jam tidak ada email yang masuk, silahkan kontak kami menggunakan email: {{contact_person}}.

Terima kasih atas kepercayaan Anda menggunakan layanan kami! Semoga sukses selalu!


Salam,


Tim {{generator_name}}

--------------------

Hi, {{user.name}}{% if user.surname %} {{user.surname}}{% endif %}!

{{generator_name}} is setting up your product, with name: {{original_product_name}}.
Please be patient, it will likely take time.
If it's completed, {{generator_name}} will send an email to you.

If after 24 hours you have not received any email, please contact us via email: {{contact_person}}.

Thank you very much for using our services.


Best Regards,


{{generator_name}} Team
