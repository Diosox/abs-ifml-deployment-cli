from jinja2 import Template


def fill_template(path, payload, write_to_file=True):
    with open(path, "r") as f:
        content = f.read()
    rendered = Template(content).render(payload)
    if write_to_file:
        with open(path, "w") as f:
            f.write(rendered)
            f.flush()
    return rendered
