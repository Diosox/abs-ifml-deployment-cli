import smtplib
import ssl
import traceback
from utils.template import fill_template


def send_email(payload, config, template_path, receiver, bcc: list = None):
    try:
        server = None
        mail_payload = dict(payload)
        mail_payload.update({
            "generator_name": config["general::generator_name"],
            "contact_person": config["email::sender_email"],
            "user": receiver
        })
        bcc = [
            x.strip() for x in config.get("email::admin_emails", "").split(",")
            if x
        ]

        message = fill_template(template_path,
                                mail_payload,
                                write_to_file=False)

        smtp_server = config["email::server"]
        port = int(config["email::port"])
        sender_email = config["email::sender_email"]
        sender_password = config["email::sender_password"]

        if config["email::type"].lower() == "tls":
            context = ssl.create_default_context()
            server = smtplib.SMTP(smtp_server, port)
            server.ehlo()
            server.starttls(context=context)
        elif config["email::type"].lower() == "ssl":
            context = ssl.create_default_context()
            server = smtplib.SMTP_SSL(smtp_server, port, context=context)
        else:
            server = smtplib.SMTP(smtp_server, port)

        server.ehlo()
        server.login(sender_email, sender_password)
        server.ehlo()

        to = [receiver["email"]]
        if bcc:
            to += bcc
        server.sendmail(sender_email, to, message)
    except Exception:
        traceback.print_exc()
        print("Failed to send email to %s using template from: %s." %
              (receiver["email"], template_path))
    finally:
        if server:
            server.quit()
