DROP DATABASE IF EXISTS {{product_subdomain}};
DROP {% if db_type.lower() == "postgresql" %}ROLE{% else %}USER{% endif %} IF EXISTS {{product_subdomain}};
{% if db_type.lower() != "postgresql" %}FLUSH PRIVILEGES;{% endif %}
