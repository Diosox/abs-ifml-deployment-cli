# ABS - IFML Deployment CLI

Reliable Software Engineering, Faculty of Computer Science, University of Indonesia (2019)

----------
## Table of Content
1.  [Initialization](#initialization)
    - [Step 1: OS Specific Instructions](#step-1-os-specific-instructions)
      - [Linux](#linux)
      - [Microsoft Windows](#microsoft-windows)
    - [Step 2: Python Requirements](#step-2-python-requirements)
      - [Install Packages Globally](#install-packages-globally)
      - [Use Virtual Environment](#use-virtual-environment)
    - [Step 3: NodeJS Requirements](#step-3-node-js-requirements)
    - [Step 4: Configurations](#step-4-configurations)
    - [Step 5: Google OAuth 2 API](#step-5-google-oauth-2-api)
2.  [Architecture](#architecture)
3.  [Requirements](#equirements)
4.  [Use the CLI](#use-the-cli)
    - [Sample Usage](#sample-usage)
      - [Linux](#linux)
      - [Microsoft Windows](#microsoft-windows)
    - [Parameters](#parameters)
      - [`deploy` Subcommand](#deploy-subcommand)
      - [`start` Subcommand](#start-subcommand)
      - [`refresh_nginx` Subcommand](#refresh_nginx-subcommand)
      - [`restart` Subcommand](#restart-subcommand)
      - [`stop` Subcommand](#stop-subcommand)
      - [`stop_inactive` Subcommand](#stop_inactive-subcommand)
      - [`destroy` Subcommand](#destroy-subcommand)
5. [Future Development](#future-development)
----------

## Initialization

### Step 1: OS Specific Instructions

#### Linux

1.  Install Python (If you already have python, skip this step).
    - For Debian-flavoured distro:  `sudo apt-get -y install software-properties-common # add-apt-repository dependency`.
2.  Install NGINX (if it's not yet built in).
    - For Debian-flavoured distro: `sudo apt-get -y install nginx`.
3.  Configure NGINX configuration as follows:
    - In `/etc/nginx/nginx.conf`, find for `include /etc/nginx/sites-enabled/*;` and replace it with `include [PREFERRED_CONFIG_DIR_FULLPATH]/*;`.
    - Save your `[PREFERRED_CONFIG_DIR_FULLPATH]` path to this CLI's `config.ini`, as `work_paths` > `nginx_path`.
4.  ABS Microservices products use MySQL 5.x or PostgreSQL 11.x as their main database. In Linux, you can freely choose your own way to install MySQL or PostgreSQL 11.x,
5.  Configure Sudoers (`/etc/sudoers/`) using `sudo visudo` by adding these two lines of code:
    ```
    Cmnd_Alias NGINX_RELOAD = /usr/sbin/nginx
    Cmnd_Alias SYSTEMCTL = /bin/systemctl
    Cmnd_Alias CERTBOT = /usr/local/bin/certbot
    www-data ALL=(ALL) NOPASSWD:NGINX_RELOAD,SYSTEMCTL,CERTBOT
    ```
    **ATTENTION: `www-data` is default user for NGINX. Please look at your `/etc/nginx/nginx.conf` for current NGINX username.**
6.  For Ubuntu version below 18.10 or Debian version below 10: Please change your default `python3` to Python 3.6 or higher.
    - You can freely choose your own source of Python installation (either use PPA or manually compile).
    - Run this command (sample code is when using Python 3.7):
      ```
      sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2
      sudo update-alternatives --config python3
      ```
      After that, choose `/usr/bin/python3.7`.

#### Microsoft Windows

1.  Download [NGINX for Windows](http://nginx.org/en/download.html), then extract it to somewhere else (preferrably `_nginx` folder inside CLI tool root folder).
2.  Inside that NGINX instance's folder, create `sites` folder.
3.  Replace `[YOUR_NGINX_DIRECTORY_HERE]\conf\nginx.conf` content with this configuration (replace `[YOUR_NGINX_DIRECTORY_HERE]` with your current base directory).
    ```
    worker_processes  1;
    events {
        worker_connections  1024;
    }
    http {
        include             mime.types;
        default_type        application/octet-stream;
        sendfile            on;
        keepalive_timeout   65;

        # to read external configuration
        include "[YOUR_DIRECTORY_HERE]/_nginx/sites/*.conf";
    }
    ```

### Step 2: Python Requirements

Install packages required by this tool, using `pip`.

#### Install Packages Globally

If you need to install packages globally, you can use `pip install -r requirements.txt`. Note that this operation needs administrator permission. You can use "Run as Administrator" in Windows, or `sudo` in Linux.

#### Use Virtual Environment

If you want to install via Virtual Environment, use this command first: `python -m venv .env` (In Linux, use `python3`). Remember to activate the virtual environment everytime you want to use this tool. In Windows, you can activate it using: `.env\Scripts\activate`, meanwhile in Linux: `.env/bin/activate`.


### Step 3: NodeJS Requirements

You must install `yarn` globally. This tool now uses `yarn` instead of `npm` for faster deployment process. To install `yarn`, use this command: `npm install -g yarn`.


### Step 4: Configurations

Rename `config.sample.ini` to `config.ini` to make the sample as your default configuration. There are some options that you can configure using `config.ini`:

**TIPS: To refer to your user folder, use `~/[desired_folder]`. To refer to current working directory, just use `[desired_folder]` without any beginning slash. To refer to another location, use absolute path!**

1.  `general` section.
    - `generator_name`: product generator's name. Used for communication (email) purposes. No default is provided, you must define it!
    - `generator_codename`: product generator's codename. Used for analytics purposes, and to differentiate each product's responsible generator. No default is provided, you must define it!
    - `generator_home_address`: address of product generator's homepage website. Used for analytics script purposes. No default is provided, you must define it!
    - `google_analytics_id`: Google Analytics Tracking ID for Cross-Domain tracking that covers all products generated with this configuration. No default is provided, you must define it!
    - `domain_name`: deployed product's domain. Used for subdomain reverse proxy. Default is `localhost`.
    - `generate_ssl_certificate`: this makes `--enable-ssl` option in `deploy` subcommand (see ["`deploy` Subcommand"](#deploy-subcommand) section for more information) runs Certbot to generate ssl certificate. Choices are boolean: `true`, `false`. Default is `false`.
2.  `work_paths` section.
    - `deploy_location`: root folder for all of your deployed products. Default is `~` (`~` is a symbol for your user folder: `/home/[user_name]/` in Linux, or `C:\Users\[user_name]\ ` in Windows). **WARNING: If you moved the deploy location after you deploy some products, make sure to move them to the new location!**
    - `nginx_location`: root folder of your NGINX installation. This setting only affects Microsoft Windows users (Linux users have their own absolute NGINX configuration folder: `/etc/nginx/`). For Microsoft Windows users, the default is `_nginx` (inside current working directory).
    - `node_location`: a shared folder that will contain `node_modules` and `yarn.lock`. It is recommended to set the directory to save disk space if you have many products, by creating a symbolic link for `node_modules` and `yarn.lock` to this shared folder. If it has not been set, Node.js modules will be installed independently for each product. The default is `_node` (inside current working directory).
    - `log_backup_location`: a shared folder that will contain log backups for all failed deployment. Every failed deployment will be destroyed so that logs need to be backed up for debugging purposes. The default is `_log_backup` (inside current working directory).
3.  `source_paths` section.
    - `backend`: location of your [ABS Microservices](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/abs-microservices) backend base (template) folder. Default is `_base/abs/framework` (inside current working directory).
    - `frontend`: location of your [IFML PWA Angular/React Transformation Tool](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/ifml-pwa-transformation-tool) frontend base (template) folder. Default is `_base/ifml` (inside current working directory).
    - `admin`: location of your [Static Page Admin](https://gitlab.com/adrika-novrialdi/static-page-backend) base folder. Default is `_base/admin` (inside current working directory).
4.  `database` section.
    - `type`: DB server type. Choices are `mysql` and `postgresql`. Default is `postgresql`.
    - `host`: DB server host address. Default is `localhost`.
    - `port`: DB server host port. Default is `3306`.
    - `root_username`: DB server superuser account name. Default is `root`.
    - `root_password`: DB server superuser password. Default is empty string.
    - `root_database`: main database name for administrations. Default is `splelive_administration`.
5.  `mail` section.
    - `server`: SMTP server address.
    - `sender_email`: email account that will be used as the sender.
    - `sender_password`: sender account's password. **CAUTION! Please keep this secret. Place your config file carefully to avoid email account abuse.**
    - `type`: SMTP connection encryption type. Choices are `TLS`, `SSL`, and `unsecure`. Default is `TLS`.
    - `port`: SMTP server's connection port. Common cases are TLS using port 587, SSL using port 465, and unsecure using 25. No default is provided.
    - `admin_emails`: list of admin email addresses (optional). All mails sent by CLI will be forwarded (bcc) to admins.
6.  `logs` section. **CAUTION: Locations defined in this section must be a relative path. It will refer to each of your deployed instances, not your user folder, CLI's root folder, or current working directory. Please keep this in mind.**
    - `backend_deploy`: Backend deployment logs location. Default is `logs/abs_backend_deployment.log`.
    - `backend_run`: Backend in-execution logs location. Default is `logs/abs_backend_execution.log`.
    - `frontend_deploy`: Frontend deployment logs location. Default is `logs/ifml_frontend_deployment.log`.
    - `frontend_run`: Frontend in-execution logs location. Default is `logs/ifml_frontend_execution.log`.
    - `admin_deploy`: Admin (Flask) deployment logs location. Default is `logs/flask_admin_deployment.log`.
    - `admin_run`: Admin (Flask) in-execution logs location. Default is `logs/flask_admin_execution.log`.
    - `nginx_deploy`: NGINX configuration deployment logs location. Default is `logs/nginx_proxy_deployment.log`.
    - `nginx_run`: NGINX in-execution logs location. This setting only affects Microsoft Windows users. Default is `logs/nginx_proxy_execution.log`.
    - `nginx_access`: NGINX access logs location. Default is `logs/nginx_proxy_access.log`.
    - `nginx_error`: NGINX error logs location. Default is `logs/nginx_proxy_error.log`.


### Step 5: Google OAuth 2 API

Tutorial coming soon.


## Architecture
![Use case in AISCO](./docs/cli-arch.png)

## Requirements

Python 3.6 or higher with Jinja2 and several IFML related packages (see `requirements.txt`)


## Use the CLI

### Sample Usage

These are sample usage of this CLI, for each supported operating systems. Details will be explained in Parameters subsection.

To run this CLI, **you must have an administrator privilege**.

#### Linux

```bash
python3 runner.py -cfg config.ini deploy -mnt --roles-file "roles.sample.json" -auth "google" --auth-id "9999-abcdef.apps.googleusercontent.com" --subdomain cattoranger "The Catto Ranger" "Products.sample.abs" "user_data.sample.json"
```
Common usage doesn't need root privileges. Enabling "enable SSL" option needs root privileges.

#### Microsoft Windows

For Microsoft Windows, use "Run as Administrator" context menu to run Command Prompt or PowerShell.

```powershell
python3 runner.py -cfg config.ini deploy -mnt --roles-file "roles.sample.json" -auth "google" --auth-id "9999-abcdef.apps.googleusercontent.com" --subdomain cattoranger "The Catto Ranger" "Products.sample.abs" "user_data.sample.json"
```


### Parameters

There are four main subcommands in this CLI. But before we continue to that, there are some keyword arguments, that if declared, it must be placed **BEFORE** subcommands and any other specific arguments.

-  `-cfg` or `--config-file` : configuration file location. Default is `~/abs_ifml_config.ini` (`~` is a symbol for your user folder).

#### `deploy` Subcommand

This subcommand is intended for new product deployment. This subcommand will deploy your new product and immediately run it afterwards. Existing product **MUST NOT BE** redeployed directly using this command.

1.  Keyword arguments (all keyword arguments must be placed **BEFORE** positional arguments, and no specific argument order required between keyword arguments).
    -  `-fe` or `--front-end`         : choose frontend (JavaScript) framework for the generated product (choice: react, angular; default: angular).
    -  `-mnt` or `--maintenance-mode` : start in maintenance mode (this is boolean argument).
    -  `-sub` or `--subdomain`        : your product's subdomain name (default: your original product name, lowercased, without spaces and punctuations).
    -  `--roles-file [json_file]`     : file that defines user emails and roles (see `roles.sample.json`).
    -  `--roles [escaped_json]`       : manually inputted json that defines user email and roles, don't forget to escape things out! (will be ignored if `--roles-file` declared)
    -  `-auth` or `--auth-type`       : define your OAuth 2 authentication method (choice: auth0, google, facebook, disabled).
    -  `--auth-id`                    : your OAuth 2 client ID to enable OAuth 2 login for the website (see ["Step 5: OAuth 2 API Setup" tutorial](#step-5-oauth-2-api-setup) for more information). Will be ignored if `-auth` or `--auth-type` is not declared or declared as `disabled` (default: empty string). <u>If your authentication method is Auth0</u>, this parameter must be filled using your Auth0 domain.
    -  `--auth-secret`                : your OAuth 2 client secret to enable OAuth 2 login for the website (see ["Step 5: OAuth 2 API Setup" tutorial](#step-5-oauth-2-api-setup) for more information). Not all OAuth 2 methods require client secret (but Facebook requires it). **ATTENTION: Keep this value secret!** Will be ignored if `-auth` or `--auth-type` is not declared or declared as `disabled` (default: empty string). <u>If your authentication method is Auth0</u>, this parameter must be filled using your Auth0 API Client ID.
    -  `-ssl` or `--enable-ssl`       : enable SSL certificate for the website so it can be accessed via HTTPS (default: False). If `generate_ssl_certificate` option in your configuration file turned on (see ["Step 4: Configurations" tutorial](#step-4-configurations)), this will run Certbot to generate SSL certificate for you. **ATTENTION: `sudo` is required if you enable Certbot!**
    -  `-prs` or `--preserve-source`  : preserve originally generated source code after deployment (default: False). If this option is True, CLI will not clean all source codes from the deployment folder.
2.  Positional arguments (must be placed **IN ORDER**, after keyword arguments).
    1. `product_name`   : your product name.
    2. `products_file`  : your products specification file (see `Products.abs`).
    Notes on products file: if your product name isn't found inside the file, the first feature configuration defined in the file will be used, while preserving your own `product-name`.
    3. `user_data_file` : admin user's data in JSON, will be used for emails.

#### `start` Subcommand

This subcommand is intended to run existing products. **Make sure that products ARE already deployed AND currently not running**.

1.  Positional arguments (must be placed **IN ORDER**).
    1. `product_name` : your product name.

#### `refresh_nginx` Subcommand

This subcommand is intended to refresh or update NGINX configuration of an existing product. **Make sure that products ARE already deployed**.

1.  Keyword arguments (all keyword arguments must be placed **BEFORE** positional arguments, and no specific argument order required between keyword arguments).
    - `-mnt` or `--maintenance-mode` : update NGINX configuration to maintenance mode (this is boolean argument, if not set, NGINX configuration will be in live mode).
2.  Positional arguments (must be placed **IN ORDER**, after keyword arguments).
    1. `product_name` : your product name.

#### `restart` Subcommand

This subcommand is intended to restart currently running (can be partially or fully running) product instances. It basically runs `stop` then `start` command. **Make sure that products ARE already deployed**.

1.  Positional arguments (must be placed **IN ORDER**).
    1. `product_name` : your product name.

#### `stop` Subcommand

This subcommand is intended to stop currently running product instances. **Make sure that products ARE already deployed**.

1.  Positional arguments (must be placed **IN ORDER**).
    1. `product_name` : your product name.

#### `stop_inactive` Subcommand

This subcommand is intended to stop currently running but inactive product instances. **Make sure that products ARE already deployed**.

1.  Positional arguments (must be placed **IN ORDER**).
    1. `interval` : time interval of inactivity tolerance, e.g. `5h` for 5 hours of maximum inactivity. Available units are `w` (week), `d` (day), `h` (hour), `m` (minute), and `s` (second).
       <blockquote>Currently this argument only accepts single time unit. It will accept "5h" but it will not accept "5h15s".</blockquote>

#### `update` Subcommand

This subcommand is intended to update existing product instances. **Make sure that products ARE already deployed**.

1.  Keyword arguments (all keyword arguments must be placed **BEFORE** positional arguments, and no specific argument order required between keyword arguments).
    -  `-f` or `--frontend`          : update the frontend instance
    -  `-b` or `--backend`           : update the backend instance
    -  `-a` or `--admin`             : update the admin API instance
    -  `-prs` or `--preserve-source` : preserve originally generated source code after deployment (default: False). If this option is True, CLI will not clean all source codes from the deployment folder.
2.  Positional arguments (must be placed **IN ORDER**, after keyword arguments).
    1. `product_name`   : your product name.

#### `destroy` Subcommand

This subcommand is intended to remove all artifacts, applications, and data of existing products. **Make sure that products ARE already deployed! Be careful with this command!**

1.  Positional arguments (must be placed **IN ORDER**).
    1. `product_name` : your product name.


## Future Development

This list of future development is tentative, will always be synchronized with current work/maintenance progress.

1.  Finalization and accomodating requirement changes (because this is part of a research!)
