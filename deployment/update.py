from multiprocessing.pool import ThreadPool
from deployment import initiate_instance_objects
from utils.administration import check_product_exists, read_json_payload


def run_command(args, config):
    if not check_product_exists(args, config):
        raise FileNotFoundError(
            "Project %s not found. Please check deployment path in your config.ini!"
            % (args.product_name, ))

    payload = read_json_payload(args.product_name, config)
    instances = initiate_instance_objects(payload, config)

    build_success = __update_instances(args, instances)
    if not build_success:
        return __revert_failed_updates(args, instances)


def __update_instances(args, instances):
    thread_pool = ThreadPool(processes=3)
    build_threads = {}
    if args.backend:
        build_threads["backend"] = thread_pool.apply_async(
            instances["backend"].update,
            kwds={"preserve_source": args.preserve_source})
    if args.frontend:
        build_threads["frontend"] = thread_pool.apply_async(
            instances["frontend"].update,
            kwds={"preserve_source": args.preserve_source})
    if args.admin:
        build_threads["admin"] = thread_pool.apply_async(
            instances["admin"].update)

    failed = []
    for key in build_threads:
        build_success = build_threads[key].get()
        if not build_success:
            failed.append(key)
            print("Update failed on instances: %s" % (key, ))
    return not failed


def __revert_failed_updates(args, instances):
    if args.backend:
        instances["backend"].revert()
    if args.frontend:
        instances["frontend"].revert()
    if args.admin:
        instances["admin"].revert()
