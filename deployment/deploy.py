import os
from multiprocessing.pool import ThreadPool
from deployment import initiate_instance_objects
from deployment.destroy import run_command as destroy_product
from deployment.start import start_non_nginx_instances
from utils.administration import (APP_ROOT_PATH, build_payload,
                                  build_user_data, check_product_exists,
                                  write_json_payload, register_product)
from utils.mail import send_email
from utils.ports import assign_ports


def run_command(args, config):
    if check_product_exists(args, config):
        raise ValueError("Project %s already exists!" % (args.product_name, ))
    sockets = assign_ports(["frontend", "backend", "admin", "nginx"])

    payload = build_payload(args, config, sockets)
    write_json_payload(payload, config)
    user = build_user_data(args)
    instances = initiate_instance_objects(payload, config)

    send_email(
        payload, config,
        os.path.join(APP_ROOT_PATH, "templates",
                     "progress_email.template.txt"), user)

    nginx_build_success = instances["nginx"].build()
    if nginx_build_success:
        instances["nginx"].run(sockets["nginx"])

    build_success = nginx_build_success and build_instances(
        instances, preserve_source=args.preserve_source)
    if not build_success:
        return __handle_failed_builds(args, payload, config, user)

    register_product(payload, user, config)
    start_non_nginx_instances(instances, sockets)
    send_email(
        payload, config,
        os.path.join(APP_ROOT_PATH, "templates",
                     "finished_email.template.txt"), user)


def build_instances(instances, preserve_source=False):
    thread_pool = ThreadPool(processes=3)
    build_threads = {
        "backend":
        thread_pool.apply_async(instances["backend"].build,
                                kwds={"preserve_source": preserve_source}),
        "frontend":
        thread_pool.apply_async(instances["frontend"].build,
                                kwds={"preserve_source": preserve_source}),
        "admin":
        thread_pool.apply_async(instances["admin"].build)
    }

    failed = []
    for key in build_threads:
        build_success = build_threads[key].get()
        if not build_success:
            failed.append(key)
            print("Build failed on instances: %s" % (key, ))
    return not failed


def __handle_failed_builds(args, payload, config, user):
    send_email(
        payload, config,
        os.path.join(APP_ROOT_PATH, "templates", "failed_email.template.txt"),
        user)
    return destroy_product(args, config)
