from components.admin import AdminInstance
from components.backend import BackendInstance
from components.frontend import FrontendInstance
from components.nginx import NginxInstance


def initiate_instance_objects(payload, config):
    return {
        "frontend": FrontendInstance(payload, config),
        "backend": BackendInstance(payload, config),
        "admin": AdminInstance(payload, config),
        "nginx": NginxInstance(payload, config)
    }
